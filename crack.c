#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

char * crackHash(char *target, char *dictionary)
{
    FILE *fp = fopen(dictionary, "r");
    char *line;
    line = malloc(1000 * sizeof(char));
    while (fgets(line, 1000, fp)) {
        char *p = strchr(line, '\n');
        if (p) {
            *p = '\0';
        }
        char *hash = md5(line, strlen(line));
        if (!strcmp(hash, target)) {
            free(hash);
            fclose(fp);
            return line;
        }
        free(hash);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *fp = fopen(argv[1], "r");
    if (!argv[1])
    {
        printf("Can't open %s for reading ", argv[1]);
    }
    
    char str[1000];
    int num = 1;
    while (fgets(str, 1000, fp)) 
    {
        char *p = strchr(str, '\n');
        if (p) *p = '\0';
        char *cracked = crackHash(str, argv[2]);
        printf("Hash: #%d %s ––> %s\n", num, str, cracked);
        num++;
        free(cracked);
    }
    fclose(fp);
}
